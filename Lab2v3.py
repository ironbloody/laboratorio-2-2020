#!/usr/bin/python
# -*- coding: utf-8 -*-

import json


def a():
    artistas.pop(0)
    print(set(artistas))
    repetidos = {}
    for i in artistas:
        if i in repetidos:
            repetidos[i] += 1
        else:
            repetidos[i] = 1
    artista = max(repetidos.keys())
    print("El artista con mas canciones es: ", artista)


def b():
    ruido.pop(0)
    enteros = [int(i) for i in ruido]
    enteros.sort()
    print(enteros)
    valor = enteros[int((len(enteros)/2)-1)] + enteros[int(len(enteros)/2)]
    mediana = int(valor/2)
    print("La mediana es: ", mediana)
    print(archivo)
    for i in archivo:
        if int(i[7]) == mediana:
            print("La cancion: ", i[1], "esta dentro de la mediana")


def c():
    bailables.pop(0)
    beats.pop(0)
    enterosbailables = [int(i) for i in bailables]
    enterosbeats = [int(i) for i in beats]
    enterosbeats.sort()
    enterosbailables.sort(reverse=True)
    bailable1 = enterosbailables[0]
    bailable2 = enterosbailables[1]
    bailable3 = enterosbailables[2]
    maslenta1 = enterosbeats[0]
    maslenta2 = enterosbeats[1]
    maslenta3 = enterosbeats[2]
    for i in archivo:
        if int(i[6]) == bailable1:
            # print("La cancion: ", i[1], "es la 1 mas bailable")
    for i in archivo:
        if int(i[6]) == bailable2:
            # print("La cancion: ", i[1], "es la 2 mas bailable")
    for i in archivo:
        if int(i[6]) == bailable3:
            # print("La cancion: ", i[1], "es la 3 mas bailable")
    for i in archivo:
        if int(i[4]) == maslenta1:
            print("La cancion: ", i[1], "es la 1 menos bailable")
    for i in archivo:
        if int(i[4]]) == maslenta2:
            print("La cancion: ", i[1], "es la 2 menos bailable")
    for i in archivo:
        if int(i[4]]) == maslenta3:
            print("La cancion: ", i[1], "es la 3 menos bailable")
    print(enterosbeats)
    print(enterosbailables)

def d(maslenta1):
    popularity.pop(0)
    enterospopularity = [int(i) for i in popularity]
    mayor = max(enterospopularity)
    for i in archivo:
        if int(i[13]) == mayor:
            print("La cancion: ", i[1], "es la de mayor popularidad")
            

if __name__ == '__main__':
    j = 0
    spotify = open('top50.csv')
    artistas = []
    archivo = []
    ruido = []
    bailables = []
    beats = []
    popularity = []
    for i, linea in enumerate(spotify):
            lista = linea.replace('\n', '').split(',')
            archivo.append(linea)
            artistas.append(lista[2])
            ruido.append(lista[7])
            bailables.append(lista[6])
            beats.append(lista[4])
            popularity.append(lista[13])
    a()
    b()
    c()
    d()
    spotify.close()
