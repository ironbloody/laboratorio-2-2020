#!/usr/bin/python
# -*- coding: utf-8 -*-

import json


def a(artistas):
    print(set(artistas))
    repetidos = {}
    for i in artistas:
        if i in repetidos:
            repetidos[i] += 1
        else:
            repetidos[i] = 1
    artista = max(repetidos.keys())
    print("El artista con mas canciones es: ", artista)


def b(ruido):
    if ruido[i] == ''"Loudness..dB.."'':
        del ruido[0]
    print(ruido)


if __name__ == '__main__':

    spotify = open('top50.csv')
    artistas = []
    archivo = []
    ruido = []
    for i, linea in enumerate(spotify):
        lista = linea.split(',')
        archivo.append(linea)
        artistas.append(lista[2])
        ruido.append(lista[7])

    a(artistas)
    b(ruido)
    spotify.close()
